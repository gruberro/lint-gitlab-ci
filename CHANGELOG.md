## [1.1.1](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/compare/v1.1.0...v1.1.1) (2020-04-04)


### Bug Fixes

* valid cli route exited with a non zero exit code ([a86e32f](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/commit/a86e32f74abfd09405e5d43e901ea4afde618cff))

# [1.1.0](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/compare/v1.0.4...v1.1.0) (2020-04-04)


### Features

* added license and updated package.json ([04aa442](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/commit/04aa44206c9a80bffe65aec317a2248c96400538))

## [1.0.4](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/compare/v1.0.3...v1.0.4) (2020-04-04)

## [1.0.3](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/compare/v1.0.2...v1.0.3) (2020-04-04)


### Bug Fixes

* added chalk colour to cli error ([2ed3c1b](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/commit/2ed3c1bb43f7d4f177898a882bba9e843d8db823))
* added cli usage to readme ([109fd64](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/commit/109fd64f3df6e8e1900ad719fbb0048e69ae9244))

## [1.0.2](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/compare/v1.0.1...v1.0.2) (2020-04-04)


### Bug Fixes

* bumped packages versions ([1f05903](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/commit/1f059039460f39f7fb4c802b91c309d17d42ee42))

## [1.0.1](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/compare/v1.0.0...v1.0.1) (2020-04-04)

### Bug Fixes

-   options default to empty object if undefined ([e9a6b9c](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/commit/e9a6b9c7c419c3bcb724f9f7ab3fe9bcd2d20c23))

# 1.0.0 (2020-04-04)

### Bug Fixes

-   bumped version to trigger release ([22990b1](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/commit/22990b1313c79da8a3cd524e8dc509c66cc3d96f))

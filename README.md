# Lint GitLab CI

A module and cli that lints a `.gitlab-ci.yml` file, using the [GitLab ci lint api](https://docs.gitlab.com/ee/api/lint.html).

## Installing

```sh
npm install -g lint-gitlab-ci
```

## Module

### Basic usage

```ts
import lintGitlabCi from "lint-gitlab-ci";

lintGitlabCi().then((result) => {
    if (result.isValid) {
        console.log("Yay!");
    } else {
        console.log("Nay!");
    }
});
```

### Options

```ts
import lintGitlabCi, { ILintGitlabCiOptions } from "lint-gitlab-ci";

const options: ILintGitlabCiOptions = {
    filePath: "example-ci.yml",
    api: {
        baseUrl: "https://example.com/api",
        version: "exampleVersion",
        paths: {
            lintCiYml: "example/path",
        },
    },
};

lintGitlabCi(options).then((result) => {
    if (result.isValid) {
        console.log("Yay!");
    } else {
        console.log("Nay!");
    }
});
```

## Cli

### Cli basic usage

```sh
lint-gitlab-ci
```

This will lint a `.gitlab-ci.yml` file and output any errors.

#### Specify file

To specify a different ci file, pass the filename as an argument.

```sh
lint-gitlab-ci example-ci.yml
```

### Cli options

#### `-u, --url`

Specifies the GitLab api url to use - defaults to `https://gitlab.com/api`.

```sh
lint-gitlab-ci --url https://example.com/api
```

#### `-a, --api-version`

Specifies the GitLab api version to use - defaults to `v4`.

```sh
lint-gitlab-ci --api-version exampleVersion
```

#### `-p, --ci-lint-path`

Specifies the GitLab api ci linting path to use - defaults to `ci/lint`.

```sh
lint-gitlab-ci --ci-lint-path example/path
```

## Integration with [husky](https://github.com/typicode/husky)

Install `lint-gitlab-ci` and `husky`.

```sh
npm install -D lint-gitlab-ci husky
```

Configure `husky` to run the `lint-gitlab-ci` cli before committing.

```json
{
    "husky": {
        "hooks": {
            "pre-commit": "lint-gitlab-ci"
        }
    }
}
```

This will validate the current `.gitlab-ci.yml` and block the commit if the file is invalid.

## Credit

This project started as a `TypeScript` implementation of [this project](https://github.com/BuBuaBu/gitlab-ci-lint), but is now a completely new implementation.

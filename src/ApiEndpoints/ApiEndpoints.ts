import axios from "axios";
import IApiEndpoints from "../framework/IApiEndpoints";
import IApiPaths from "../framework/IApiPaths";
import IApiLintResponse from "../models/IApiLintResponse";

interface IEnpointsProps {
    rootUrl: string;
    apiVersion: string;
    paths?: IApiPaths;
}

export default class ApiEndpoints implements IApiEndpoints {
    private _paths: IApiPaths | undefined;

    constructor(props: IEnpointsProps) {
        const { rootUrl, apiVersion, paths } = props;

        this._paths = paths;

        axios.defaults.baseURL = `${this.removeSlash(rootUrl)}/${this.removeSlash(apiVersion)}`;
    }

    public async lintCiYml(content: string): Promise<IApiLintResponse> {
        let url = this._paths?.lintCiYml || "/ci/lint";

        const response = await axios.post(url, { content: content });

        return response.data as IApiLintResponse;
    }

    private removeSlash(str: string) {
        return str
            .replace(/^\/*/gm, "") // remove slash prefix
            .replace(/\/*$/gm, ""); // remove slash suffix;
    }
}

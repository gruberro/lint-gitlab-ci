import ApiEndpoints from "./ApiEndpoints";
import axios, { AxiosRequestConfig } from "axios";
import MockAdapter from "axios-mock-adapter";

// This sets the mock adapter on the default instance
var mock = new MockAdapter(axios);

describe("ApiEndpoints", () => {
    const rootUrl = "rootUrl";
    const apiVersion = "apiVersion";
    const baseUrl = `${rootUrl}/${apiVersion}`;

    const lintCiYml = "/ci/lint";

    const props = {
        rootUrl,
        apiVersion,
    };

    describe("baseUrl", () => {
        const content = "content";

        beforeEach(() => {
            mock.onPost(lintCiYml, { content: content }).reply((config: AxiosRequestConfig) => {
                if (config.baseURL === baseUrl) return [200, {}];

                return [500, {}];
            });
        });

        it("should remove unecessary slashes from baseUrl", async () => {
            const slashyRootUrl = `//${rootUrl}///`;
            const slashyApiVersion = `/////${apiVersion}///`;

            const endpoints = new ApiEndpoints({
                rootUrl: slashyRootUrl,
                apiVersion: slashyApiVersion,
            });

            await endpoints.lintCiYml(content);
        });
    });

    describe("lintCiYml", () => {
        const content = "content";
        const mockedResponse = "CI_LINT_POST";

        beforeEach(() => {
            mock.onPost(lintCiYml, { content: content }).reply(200, mockedResponse);
        });

        it("should return axios promise", async () => {
            const endpoints = new ApiEndpoints(props);

            const result = await endpoints.lintCiYml(content);

            expect(result).toEqual(mockedResponse);
        });
    });
});

#!/usr/bin/env node

// https://itnext.io/how-to-create-your-own-typescript-cli-with-node-js-1faf7095ef89

import { Command } from "commander";
import lintGitlabCi, { ILintGitlabCiOptions } from ".";
import defaultOptions from "./defaultOptions";
import figlet from "figlet";
import chalk from "chalk";

var pjson = require("../package.json");
const programHexColor = "#F80";

const program = new Command();

program
    .version(pjson.version, "-v, --version", "Output the current version")
    .name(pjson.name)
    .description(
        `${chalk.hex(programHexColor)(figlet.textSync(pjson.name, { horizontalLayout: "full" }))}

A module that lints a .gitlab-ci.yml file, using the GitLab api.
    `
    )
    .usage("[options] <file>")
    .option("-u, --url <url>", "Specify the GitLab api URL", defaultOptions.api.baseUrl)
    .option(
        "-a, --api-version <api-version>",
        "Specify the GitLab api version",
        defaultOptions.api.version
    )
    .option(
        "-p, --ci-lint-path <ci-lint-path>",
        "Specify the GitLab api ci linting path",
        defaultOptions.api.paths.lintCiYml
    )
    .helpOption("-h, --help", "Display help information")
    .parse(process.argv);

program.parse(process.argv);

const options: ILintGitlabCiOptions = {
    filePath: program.args[0] || defaultOptions.filePath,
    api: {
        baseUrl: program.url,
        version: program.apiVersion,
        paths: {
            lintCiYml: program.ciLintPath,
        },
    },
};

lintGitlabCi(options)
    .then((result) => {
        console.log(chalk.hex(programHexColor)(`${pjson.name}:`));

        if (result.isValid) {
            console.log(chalk.green("No gitlab ci lint errors"));
            process.exit(0);
        } else {
            result.errors.forEach((error) => console.error(chalk.red(error)));
            process.exit(1);
        }
    })
    .catch((err) => {
        console.error(chalk.red(err.message));
        process.exit(1);
    });

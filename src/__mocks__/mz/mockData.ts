export interface IMockFiles {
    "fileName.yml": string;
    "fileName.txt": string;
    notAString: number;
}

export const mockFiles: IMockFiles = {
    "fileName.yml": "file.yml contents",
    "fileName.txt": "file.txt contents",
    notAString: 2,
};

import { mockFiles, IMockFiles } from "./mockData";

export const readFile = jest.fn((fileName: string) => {
    return new Promise((resolve, reject) => {
        const file = mockFiles[fileName as keyof IMockFiles];

        typeof file !== "undefined"
            ? resolve(file)
            : reject({
                  error: "File " + fileName + " not found.",
              });
    });
});

export default interface ILintResponse {
    isValid: boolean;
    errors: string[];
}

import ILinter from "../ILinter";

export const MOCK_LINTER_RESPONSE = "MOCK_LINTER_RESPONSE: ";

const mockLinter: ILinter = {
    //@ts-ignore
    lintCiYml: jest.fn(() => MOCK_LINTER_RESPONSE),
};

export default mockLinter;

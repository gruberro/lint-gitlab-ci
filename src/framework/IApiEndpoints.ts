import IApiLintResponse from "../models/IApiLintResponse";

export default interface IApiEndpoints {
    lintCiYml: (content: string) => Promise<IApiLintResponse>;
}

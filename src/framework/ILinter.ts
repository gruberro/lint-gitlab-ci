import ILintResponse from "../models/ILintResponse";

export default interface ILinter {
    lintCiYml: (fileName: string) => Promise<ILintResponse>;
}
